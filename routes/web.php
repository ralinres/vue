<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

    Route::get('title/index', 'TitleController@index');
    Route::post('title/store', 'TitleController@store');
    Route::delete('title/delete/{id}', 'TitleController@delete');
    Route::put('title/edit', 'TitleController@update');