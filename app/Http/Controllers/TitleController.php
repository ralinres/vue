<?php

namespace App\Http\Controllers;

use App\Title;
use Illuminate\Http\Request;

class TitleController extends Controller
{

    public function index(){

        return Title::all();

    }

    public function store(Request $request){

        $this->validate($request, [
            'name'   => 'required|max:200',
             'value' => 'required|max:200'
        ]);

        $title = new Title();
        $title->name = $request->name;
        $title->value= $request->value;
        $title->save();

        return response()->json($title);

    }

    public function update(Request $request){

        $this->validate($request, [
            'name'   => 'required|max:200',
            'value' => 'required|max:200'
        ]);

        $title = Title::find((int)$request->id);
        $title->name = $request->name;
        $title->value= $request->value;
        $title->save();

        return response()->json($title);

    }

    public function delete($id){

        $title = Title::find($id);
        $title->delete();

        return response()->json($title);

    }

}
